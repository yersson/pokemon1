
package pack1;

import java.util.Random;

public class Mascota {
    
   private String nombre;
   private int salud,tipo,fuerza,nivel,ph;
   
   private Random rand=new Random(System.nanoTime());
   
    //

      
    //falta asignar parte logica a las devilidades de los pokemones 

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getSalud() {
        return salud;
    }

    public void setSalud(int salud) {
        this.salud = salud;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getFuerza() {
        return fuerza;
    }

    public void setFuerza(int fuerza) {
        this.fuerza = fuerza;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getPh() {
        return ph;
    }

    public void setPh(int ph) {
        this.ph = ph;
    }

    public int ataque_tipo(){
        int ataque=rand.nextInt(2);
        return nivel*tipo*ataque;
        
    }
    
    public int ataque_fuerza(){
        if(ph>0){
            ph--;
            return nivel*fuerza; 
        } 
        else{
            return 0;
        }
    }
    public void dano(int salud){
        this.salud=salud;
        
    }
    
    
}
